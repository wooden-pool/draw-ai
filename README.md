#### 体验地址
[地址1](http://104.168.43.119:9998/)

[地址2](http://xlike.top)

---

![SD](file/SD.png)

![DThree](file/DThree.png)

#### 一个 SpringBoot+vue 项目,前端不会,将就一下吧
#### 适合新手 - 集成AI对话（未公开,怕被打）,AI绘画

**其中注意:**
```
CorsConfiguration这个类配置自己的域名/跨域配置/设置自己的请求路径
其中所有用到的方法都已经封装到了
1.Draw_DallEThreeUtil
2.Draw_StableDrawUtil
直接修改这两个工具类即可
```

```
1.Draw_DallEThreeUtil 中的 MODELS_URL 和 API_KEYS (*)
MODELS_URL 模型访问的链接 通常为 : https://api.openai.com/v1/images/generations
API_KEYS 模型访问的key 可在openai官网申请
代理路径/key也可以
```

```
2.Draw_StableDrawUtil 中的 GET_ACCESS_TOKEN
API_KEY 模型访问的链接 通常为 : https://app.prodia.com/api (可自行去申请)
GET_ACCESS_TOKEN 用作文本翻译 (可选)
```

###### 注意: 注释掉下面代码,

```
/**
注释掉 若没有申请百度的Token 需要注释 Draw_StableDrawUtil 中 以下代码
JSONObject object = JSONObject.parseObject(content);
String prompt = (String) object.get("prompt");
try {
prompt = textTrans(prompt);
} catch (Exception e) {
log.error(e.getMessage());
}
object.put("prompt", prompt);
**/
修改 为 :  String requestBodyJson = content;
```

###### vue项目

###### 1.npm install

###### 2.替换 main.js 中的  axios.defaults.baseURL="http://localhost:9999";

###### 3.npm run serve