package top.xlike.chatgptkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author 木池
 */
@SpringBootApplication
public class ChatGPTKeyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatGPTKeyApplication.class, args);
    }


}
