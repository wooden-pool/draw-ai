package top.xlike.chatgptkey.controller;

import org.springframework.web.bind.annotation.*;
import top.xlike.chatgptkey.dto.DrawingDto;
import top.xlike.chatgptkey.utils.draw.Draw_DallEThreeUtil;
import top.xlike.chatgptkey.utils.draw.Draw_StableDrawUtil;

import java.io.IOException;
import java.util.List;


/**
 * @author 木池
 */
@RestController
@RequestMapping("/draw")
public class DrawController {


    @PostMapping("/dallThree")
    public List<String> getDallThree(@RequestBody DrawingDto drawingDto) {
        return Draw_DallEThreeUtil.getDallThree(drawingDto.getPrompt(), drawingDto.getN());
    }


    @PostMapping("/stableDraw")
    public List<String> getStableDraw(@RequestBody DrawingDto drawingDto) throws IOException {
        return List.of(Draw_StableDrawUtil.generateText(drawingDto.getPrompt()));
    }


}
