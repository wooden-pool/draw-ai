package top.xlike.chatgptkey.dto;

import lombok.*;

/**
 * @author 木池
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChatNetDto {

    private String contentParam;

    private String model;

}
