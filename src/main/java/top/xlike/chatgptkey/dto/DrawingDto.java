package top.xlike.chatgptkey.dto;

import lombok.*;

/**
 * @author 木池
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DrawingDto {

    private String prompt;

    private Integer n;

}
