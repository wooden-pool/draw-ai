//package top.xlike.chatgptkey.service;
//
//import com.easy.chat.controller.ChatDto;
//import com.easy.chat.enums.ChatModelEnum;
//import com.easy.chat.service.api.ChatApiService;
//import com.easy.chat.utils.ChatMsgUtil;
//import com.easy.chat.utils.chat.Message;
//import com.easy.redis.utils.RedisUtils;
//import com.alibaba.fastjson2.JSONArray;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
///**
// * @author 木池
// */
//@Service
//public class ChatService {
//
//    private final ChatApiService chatApiService;
//
//
//    public ChatService(ChatApiService chatApiService) {
//        this.chatApiService = chatApiService;
//    }
//
//    public String chatAiNotStream(ChatDto chatDto) {
//        Integer modelNum = chatDto.getModelNum();
//        ChatModelEnum chatModelEnum = ChatModelEnum.getModelByNum(modelNum);
//        String prompt = chatDto.getPrompt();
//        String openId = chatDto.getOpenId();
//        Object object = RedisUtils.getCacheObject(openId);
//        List<Message> msgList = object == null ? ChatMsgUtil.setInitMsg(openId, prompt, chatModelEnum.getModel()) : ChatMsgUtil.setUserMsg(openId, prompt);
//        String aiResContent = chatApiService.chatSeries(msgList, chatModelEnum);
//        if (aiResContent != null) {
//            ChatMsgUtil.setAssistantMsg(openId, aiResContent);
//        }
//        return aiResContent;
//    }
//
//}
