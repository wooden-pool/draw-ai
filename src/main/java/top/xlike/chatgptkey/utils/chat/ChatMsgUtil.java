//package top.xlike.chatgptkey.utils.chat;
//
//import com.alibaba.fastjson2.JSONArray;
//import com.alibaba.fastjson2.JSONObject;
//import com.easy.chat.utils.chat.Message;
//import com.easy.chat.utils.chat.RoleEnum;
//import com.easy.redis.utils.RedisUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author 木池
// */
//public class ChatMsgUtil {
//
//    /**
//     * 初始化消息
//     *
//     * @return 消息体
//     */
//    public static List<Message> setInitMsg(String openId, String prompt, String model) {
//        List<Message> msgList = new ArrayList<>();
//        Message sysMessage = new Message(RoleEnum.system.name(), "你是强大的" + model + "，快来帮助解决我的问题吧");
//        Message userMessage = new Message(RoleEnum.user.name(), prompt);
//        msgList.add(sysMessage);
//        msgList.add(userMessage);
//        RedisUtils.setCacheObject(openId, msgList);
//        return msgList;
//    }
//
//    /**
//     * 构建用户消息
//     */
//    public static List<Message> setUserMsg(String openId, String prompt) {
//        List<Message> msgList = RedisUtils.getCacheObject(openId);
//        Message userMessage = new Message(RoleEnum.user.name(), prompt);
//        msgList.add(userMessage);
//        RedisUtils.deleteObject(openId);
//        RedisUtils.setCacheObject(openId, msgList);
//        return msgList;
//    }
//
//    public static List<Message> setAssistantMsg(String openId, String resContent) {
//        List<Message> msgList = RedisUtils.getCacheObject(openId);
//        Message userMessage = new Message(RoleEnum.assistant.name(), resContent);
//        msgList.add(userMessage);
//        RedisUtils.deleteObject(openId);
//        RedisUtils.setCacheObject(openId, msgList);
//        return msgList;
//    }
//
//
//    public static void clear(String openid) {
//        RedisUtils.deleteObject(openid);
//    }
//}
