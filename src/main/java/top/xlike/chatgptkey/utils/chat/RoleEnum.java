package top.xlike.chatgptkey.utils.chat;


/**
 * @author 木池
 */

public enum RoleEnum {
    system,
    user,
    assistant;
}