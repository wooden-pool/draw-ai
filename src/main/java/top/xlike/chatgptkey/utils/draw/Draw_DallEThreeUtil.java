package top.xlike.chatgptkey.utils.draw;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.Builder;
import lombok.Data;
import okhttp3.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author 木池
 */
public class Draw_DallEThreeUtil {

    private static final String HEADER_PREFIX = "Bearer ";
    private static final String HEAD_TYPE = "Content-Type";
    private static final String HEAD_VALUE = "application/json";
    private static final String HEADER_AUTHORIZATION = "Authorization";

    private static final Integer TIMEOUT_MAX = 5;
    private static final Integer N = 1;
    private static final String MODEL = "dall-e-3";
    private static final String SIZE = "1024x1024";

    // FIXME 替换为自己的 代理路径
    private static final String MODELS_URL = "https://api.openai.com/v1/images/generations";

    // FIXME 替换为自己的 KEY
    private static final List<String> API_KEYS = new ArrayList<>();
    static {
        API_KEYS.add("sk-示例");
        API_KEYS.add("sk-示例");
        API_KEYS.add("sk-示例");
        API_KEYS.add("sk-示例");
        API_KEYS.add("sk-示例");
        API_KEYS.add("sk-示例");
    }

    public static List<String> getApiKeys() {
        return API_KEYS;
    }


    /**
     * 不能生成 包含敏感词 的图片
     * @param prompt 提示词
     * @param n      生成的数量 - 最大不超过key的数量
     * @return urlList 图片url集合
     */
    public static List<String> getDallThree(String prompt, Integer n) {
        List<String> urlList = new ArrayList<>();
        if (n == null) {
            n = N;
        }
        if (n > getApiKeys().size()) {
            throw new RuntimeException("n不能大于"+getApiKeys().size());
        }
        ExecutorService executorService = Executors.newFixedThreadPool(n);
        List<String> apiKeys = getApiKeys();
        int numThreads = Math.min(n, apiKeys.size());
        for (int i = 0; i < numThreads; i++) {
            int finalI = i;
            executorService.submit(() -> {
                String imgUrl = sendRequest(prompt, apiKeys.get(finalI));
                if (imgUrl != null) {
                    urlList.add(imgUrl);
                }
            });
        }
        executorService.shutdown();
        try {
            // 设置请求超时时间为1分钟
            if (!executorService.awaitTermination(1, TimeUnit.MINUTES)) {
                // 如果超时，则终止线程池
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return urlList;
    }

    private static String sendRequest(String prompt, String apiKey) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_MAX, TimeUnit.MINUTES)
                .readTimeout(TIMEOUT_MAX, TimeUnit.MINUTES)
                .build();
        MediaType mediaType = MediaType.parse(HEAD_VALUE);
        ImageRequest imageRequest = ImageRequest.builder().prompt(prompt).n(N).model(MODEL).size(SIZE).build();
        String requestBodyJson = JSON.toJSONString(imageRequest);
        RequestBody body = RequestBody.create(mediaType, requestBodyJson);
        Request request = new Request.Builder()
                .url(MODELS_URL)
                .method("POST", body)
                .addHeader(HEAD_TYPE, HEAD_VALUE)
                .addHeader(HEADER_AUTHORIZATION, HEADER_PREFIX + apiKey)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() == null) {
                return null;
            }
            String responseBody = response.body().string();
            return getImgUrl(responseBody);
        } catch (IOException e) {
            return null;
        }
    }


    /**
     * 获取图片url
     *
     * @param responseBody 响应体
     * @return url
     */
    private static String getImgUrl(String responseBody) {
        JSONObject jsonObject = JSON.parseObject(responseBody);
        JSONArray dataArray = jsonObject.getJSONArray("data");
        JSONObject dataObject = dataArray.getJSONObject(0);
        return dataObject.getString("url");
    }

    @Data
    @Builder
    public static class ImageRequest {
        private String model;
        private String prompt;
        private int n;
        private String size;

        public ImageRequest(String model, String prompt, int n, String size) {
            this.model = model;
            this.prompt = prompt;
            this.n = n;
            this.size = size;
        }

    }

}
