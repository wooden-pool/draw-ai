package top.xlike.chatgptkey.utils.draw;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.Data;
import lombok.Getter;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.xlike.chatgptkey.utils.baidu.GsonUtils;
import top.xlike.chatgptkey.utils.baidu.HttpUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 木池
 */
public class Draw_StableDrawUtil {

    // FIXME 替换为自己的 KEY 可以去 https://app.prodia.com/api 申请
    private static final String API_KEY = "";
    private static final String MODELS_URL = "https://api.prodia.com/v1/sdxl/generate";
    // FIXME  ??? 替换为自己的 client_id 和  client_secret  可以去 百度API 免费领取一年
    private static final String GET_ACCESS_TOKEN = "https://aip.baidubce.com/oauth/2.0/token?client_id=???&client_secret=???&grant_type=client_credentials";


    private static final String HEAD_TYPE = "Content-Type";
    private static final String HEAD_ACCEPT = "accept";
    private static final String HEAD_VALUE = "application/json";
    private static final String HEADER_X_PRODIA_KEY = "X-Prodia-Key";
    private static final Integer TIMEOUT_MAX = 5;
    private static final String BASE_URL = "https://api.prodia.com/v1/job/%s";
    private static final long POLL_INTERVAL_MS = 3000;

    private static final String TRANSLATORS_API_URL = "https://aip.baidubce.com/rpc/2.0/mt/texttrans/v1";

    private static final OkHttpClient CLIENT = new OkHttpClient.Builder()
            .callTimeout(TIMEOUT_MAX, TimeUnit.MINUTES)
            .readTimeout(TIMEOUT_MAX, TimeUnit.MINUTES)
            .build();
    private static final Logger log = LoggerFactory.getLogger(Draw_StableDrawUtil.class);



    public static String generateText(String content) throws IOException {
        JSONObject object = JSONObject.parseObject(content);
        String prompt = (String) object.get("prompt");
        try {
            prompt = textTrans(prompt);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        object.put("prompt", prompt);
        String requestBodyJson = object.toJSONString();
        MediaType mediaType = MediaType.parse(HEAD_VALUE);
        Request request = new Request.Builder()
                .url(MODELS_URL)
                .post(RequestBody.create(mediaType, requestBodyJson))
                .addHeader(HEAD_ACCEPT, HEAD_VALUE)
                .addHeader(HEAD_TYPE, HEAD_VALUE)
                .addHeader(HEADER_X_PRODIA_KEY, API_KEY)
                .build();
        try (Response response = CLIENT.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                JSONObject jsonObject = JSONObject.parseObject(response.body().string());
                String jobId = jsonObject.getString("job");
                return getJobDetails(jobId);
            }
            throw new IOException("Request failed: " + response.code() + " " + response.message());
        } catch (InterruptedException e) {
            return e.getMessage();
        }
    }

    @Data
    public static class ImageGenerationRequest {
        private final String model = "sd_xl_base_1.0.safetensors [be9edd61]";
        private final String negativePrompt = "badly drawn";
        private final int steps = 20;
        private final int cfgScale = 7;
        private final String sampler = "DPM++ 2M Karras";
        private final int width = 1024;
        private final int height = 1024;

        private String prompt;
        private final String stylePreset;

        public ImageGenerationRequest(String prompt, StylePresetSampler stylePresetSampler) {
            this.prompt = prompt;
            this.stylePreset = stylePresetSampler.getCode();
        }

        @Getter
        public enum StylePresetSampler {
            CINEMATIC("cinematic", "电影"),
            ThreeMODEL("3d-model", "3d模型"),
            ANALOG_FILM("analog-film", "胶片"),
            ANIME("anime", "动漫"),
            NEON_PUNK("neon-punk", "霓虹朋克");
            private final String code;
            private final String value;

            StylePresetSampler(String code, String value) {
                this.code = code;
                this.value = value;
            }
        }
    }


    /**
     * 获取任务详情
     *
     * @param jobId 任务ID
     * @return 图片地址
     */
    private static String getJobDetails(String jobId) throws IOException, InterruptedException {
        // 构建请求URL
        String url = String.format(BASE_URL, jobId);
        // 构建请求
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader(HEAD_ACCEPT, HEAD_VALUE)
                .addHeader(HEADER_X_PRODIA_KEY, API_KEY)
                .build();
        boolean isComplete = false;
        String imageUrl = null;
        // 开始轮询
        while (!isComplete) {
            // 执行请求并处理响应
            try (Response response = CLIENT.newCall(request).execute()) {
                if (response.isSuccessful() && response.body() != null) {
                    // 读取响应体内容
                    String res = response.body().string();
                    JSONObject jsonObject = JSONObject.parseObject(res);
                    String status = jsonObject.getString("status");
                    // 检查作业状态
                    if ("succeeded".equals(status)) {
                        isComplete = true;
                        imageUrl = jsonObject.getString("imageUrl");
                    } else if ("failed".equals(status)) {
                        throw new IOException("Job failed: " + jobId);
                    }
                } else {
                    throw new IOException("Request failed: " + response.code() + " " + response.message());
                }
            }
            if (!isComplete) {
                Thread.sleep(POLL_INTERVAL_MS);
            }
        }

        return imageUrl;
    }




    private static String textTrans(String q) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("from", "zh");
        map.put("to", "en");
        map.put("q", q);
        String param = GsonUtils.toJson(map);
        String accessToken = getAccessToken();
        String result = HttpUtil.post(TRANSLATORS_API_URL, accessToken, HEAD_VALUE, param);
        return extractTranslation(result);
    }

    private static String getAccessToken() throws IOException {
        MediaType mediaType = MediaType.parse(HEAD_VALUE);
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(GET_ACCESS_TOKEN)
                .method("POST", body)
                .addHeader(HEAD_TYPE, HEAD_VALUE)
                .addHeader("Accept", HEAD_VALUE)
                .build();
        Response response = CLIENT.newCall(request).execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
        return jsonObject.getString("access_token");
    }
    private static String extractTranslation(String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        JSONObject resultObject = jsonObject.getJSONObject("result");
        JSONObject transResultObject = resultObject.getJSONArray("trans_result").getJSONObject(0);
        return transResultObject.getString("dst");
    }


}
