import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false


import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);


//配置axios的全局属性和基础路径
import axios from "axios";
Vue.prototype.$http = axios;
//只有发送axios请求才会加上这个基础路径
axios.defaults.baseURL="http://localhost:9999";

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

