import Vue from 'vue'
import VueRouter from 'vue-router'
import DalleThree from '../views/DrawDallThree.vue'
import ChatFileSum from '../views/ChatFileSum.vue'
import OpenAiDalleThree from '../views/OpenAiDalleThree.vue'
import ChatGPTALL from '../views/ChatGPTALL.vue'
import StableDrawing from '../views/StableDrawing.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'StableDrawing',
    component: StableDrawing,
  },
  {
    path: '/StableDrawing',
    name: 'StableDrawing',
    component: StableDrawing,
  },
  {
    path: '/ChatGPTALL',
    name: 'ChatGPTALL',
    component: ChatGPTALL,
    meta: { ChatGPTALL: true }
  },
  {
    path: '/OpenAiDalleThree',
    name: 'OpenAiDalleThree',
    component: OpenAiDalleThree,
    meta: { requiresBackground: true }
  },
  {
    path: '/ChatFileSum',
    name: 'ChatFileSum',
    component: ChatFileSum
  },
  {
    path: '/DallThree',
    name: 'DallThree',
    component: DalleThree
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresBackground)) {
    document.body.style.background = "url('./assets/bj2.jpg') no-repeat center center fixed";
    document.body.style.backgroundSize = "cover";
  } else {
    document.body.style.background = null;
    document.body.style.backgroundSize = null;
  }
  next();
});



export default router
