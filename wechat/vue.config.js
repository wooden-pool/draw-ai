const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    outputDir: 'gpt',
    // devServer: { disableHostCheck: true },
    runtimeCompiler: true,
    devServer: {
        port: 9998,
    }
})

